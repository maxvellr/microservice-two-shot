import React, { useEffect, useState } from 'react';


function CreateShoesForm () {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [photo, setPhoto] = useState('');
    const [bin, setBin] = useState('');

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePhotoChange = (event) => {
        const value = event.target.value;
        setPhoto(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.picture_url = photo;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applictaion/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoes = await response.json();
            console.log(newShoes);

            setManufacturer('');
            setName('');
            setColor('');
            setPhoto('');
            setBin('');



        }
    }

const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setBins(data.bins)
    }
}

useEffect(() => {
    fetchData();
}, []);


    return(
        <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a pair of shoes</h1>
                        <form onSubmit={handleSubmit} id="create-shoes-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="Brand" value={manufacturer} required type="text" autoComplete="off" name="manufacturer" id="manufacturer" className="form-control"/>
                            <label htmlFor="name">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" autoComplete="off" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" value={color} required type="text" autoComplete="off" name="color" id="color" className="form-control"/>
                            <label htmlFor="name">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhotoChange} placeholder="Photo" value={photo} required type="text" autoComplete="photo" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="name">Photo URL</label>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Select Locaton</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.closet_name} value={bin.id}>
                                        {bin.closet_name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>

                        </form>
                        </div>
                </div>
            </div>

    );

}
export default CreateShoesForm
