import { useEffect, useState } from 'react';
import HatForm from './HatForm';

function HatsList() {
  const [hats, setHats] = useState([]);
  const [showForm, setShowForm] = useState(false);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:8090/api/hats/${id}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      setHats(hats.filter(hat => hat.id !== id));
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFormSubmit = () => {
    setShowForm(false);
    getData();
  };

  return (
    <div>
      <button onClick={() => setShowForm(true)}>Create New Hat</button>
      {showForm && <HatForm onFormSubmit={handleFormSubmit} />}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
            <th>Picture</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => (
            <tr key={hat.id}>
              <td>{hat.style_name}</td>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
              <td>{hat.location.closet_name}</td>
              <td>
                <img src={hat.picture_url} alt={hat.style_name} style={{width: "50px", height: "50px"}} /> 
              </td>
              <td>
                <button onClick={() => handleDelete(hat.id)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default HatsList;
