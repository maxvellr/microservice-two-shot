import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ShoesList() {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  };

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${id}`,{
        method:'DELETE',
    })
    if(response.ok){
        console.log({"Deleted": true})
        setShoes(shoes.filter(shoe => shoe.id !== id));
    }
  };

  useEffect(()=>{
    getData()
  }, [])


  return (
    <div className="container px-4 py-4 tex-center">
      <h1>My Shoes</h1>
      <div className="d-grid gap-2 d-sm-flex justify-content-md-end">
        <Link to="/shoes/new" className="btn btn-info btn-sm px-4 gap-3">+ Shoes</Link>
      </div>
        <table className="table table-sm align-middle">
          <thead>
            <tr>
              <th>Brand</th>
              <th>Style</th>
              <th>Color</th>
              <th>Location</th>
              <th>Picture</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.id} >
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.name }</td>
                  <td>{ shoe.color }</td>
                  <td>{ shoe.bin }</td>
                  <td>
                    <img className="img-fluid" src={ shoe.picture_url } alt='Shoe Photo' />
                  </td>
                <td>
                <button type="button" className="btn btn-sm btn-outline-danger" onClick={() => handleDelete(shoe.id)}>Delete</button>
                </td>
                </tr>
              );
            })}
          </tbody>
        </table>

    </div>
  );
};

export default ShoesList;
