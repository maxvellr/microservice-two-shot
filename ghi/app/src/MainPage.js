import {Link} from 'react-router-dom';

function MainPage() {
  return (

  <div className="container-fluid">
    <div className="px-4 my-5 text-center gx-2">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <img src="https://images.pexels.com/photos/8851585/pexels-photo-8851585.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
    className="img-fluid"></img>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4 py-3 py-2 fw-bold">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
      <div className="d-grid gap-2 d-sm-flex justify-content-md-center">
            <Link to="/shoes" className="btn btn-secondary btn-lg px-4 col-6 gap-3 fw-bold">My Shoes</Link>
            <Link to="/hats" className="btn btn-secondary btn-lg px-4 col-6 gap-3 fw-bold">My Hats</Link>
      </div>
    </div>
  </div>
  );
}

export default MainPage;
