import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm'; // import your HatForm component
import HatsList from './HatsList'; // import your HatsList component
import ShoesList from './ShoesList';
import CreateShoesForm from './CreateShoesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatsList />} /> {/* Route for displaying the list of hats */}
          <Route path="hats/new" element={<HatForm />} /> {/* Route for creating a new hat */}
          <Route path="shoes">
              <Route index element={<ShoesList />} />
              <Route path="new" element={<CreateShoesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
