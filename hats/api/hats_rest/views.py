

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
#^^^ these three lines are wk 7
from .models import Hat, LocationVO
import json


class LocationListEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name"]


class LocationDetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "created",
        "updated",
    ]






class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color", "picture_url",
                   "id", "location"]
    encoders = {
        "location": LocationListEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    #this is where you write the code for this particular
        #function


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        location = Hat.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=HatDetailEncoder,
            safe=False,
        )
