# Wardrobify

Team:

* Max Ruhr - Shoes Microservice
* Charles Agar - Hats Microservice

## Design

Wardrobify! is an organizational tool used to catalogue a user's hats and shoes to their bins and locations, respectively.
Bins and Locations can be created through POST requests, and lists available locations and bins can be pulled through Get reuquests
to their APIs.

Shoes and Hats are listed on the webpage with unique characteristics on display, and forms to create both are located on their list pages. The user is also able to delete items from the database on their list view.

## Shoes microservice

Starting with creating a Shoe model with a bin characteristic that is a foreign key to BinVO and a BinVO model to access Bins within the database.
The poll.py file is then setup to poll for bins in the Wardrobe API. After the views are created showing the required details of each shoe, as well as their associated bins, the POST and DELETE requests are added to those views. Once backend is fully functional, the React front end work
begins.
A ShoesList.js file is created and linked to the navigation on the page, and a CreateShoesForm.js is created and linked to the list page via
an Add Shoes button. The form handles a create request with all required fields and clears upon successful POST. The list page then displays the newly
created shoe. Each shoe entry in the list page has its own delete button as well that handles deletes upon click.

## Hats microservice

Start at the beginning of conference-go in learn and try to relate conference-go to
the requests and rubric of the project.
