from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "bin",
        "id",
        "manufacturer",
        "color",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "name",
        "color",
        "bin",
        "picture_url",
    ]
    encoders ={
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    """
    Lists the shoes and their bins they are stored in
    Handles Creation of new Shoe object
    """

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_shoes(request, pk):
    """
    Returns the details for the Shoe model specified by the id parameter
    Deletes Shoe by id
    """

    if request.method == "GET":
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    else:
        try:
            shoes = Shoe.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                {"deleted":"true"},
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
